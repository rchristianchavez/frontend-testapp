import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router';
import Home from './views/Home';
import SignIn from './views/SignIn';
import SignUp from './views/SignUp';
import ListReport from './views/user/ListReportView.vue'
import Vuelidate from 'vuelidate'
import HomeUser from './views/user/HomeUserView.vue'
Vue.use(Vuelidate)

Vue.use(VueRouter);
import './scss/main.scss';
Vue.config.productionTip = false

const router = new VueRouter({
  routes: [
    { path: '/', component: Home },
    { path: '/signin', component: SignIn },
    { path: '/signup', component: SignUp },
    { path: '/listreportuser', component: ListReport },
    { path: '/homeuser', component: HomeUser },


  ],

})

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
